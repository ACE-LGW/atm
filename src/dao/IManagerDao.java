package dao;

import java.util.List;

import bean.Card;
import bean.Manager;
import bean.User;

/**
 * 管理员的接口
 * @author ligw
 * @since Sep 7, 2019
 */
public interface IManagerDao {
	//管理员登录
	public boolean login(Manager m);

	//添加用户
	public boolean addUser(User u);

	//添加银行卡
	public boolean addCard(Card c);

	//注销用户和银行卡
	public boolean logout(User u);

	//查询冻结的银行卡
	public List<Card> searchFrozenCard();

	//查询所有用户
	public List<User> findAll();

	//修改银行卡的输错次数
	public boolean updateCountByCardnumber(String cardnumber);

	//根据银行卡号查询银行卡信息
	public Card getCardInfoByCardnumber(String cardnumber);
}
