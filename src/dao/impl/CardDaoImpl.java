package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import bean.Card;
import dao.ICardDao;
import util.DbUtil;

/**
 * 银行卡接口的实现
 * @author 10234
 * @since Sep 8, 2019
 */
public class CardDaoImpl implements ICardDao {
	/**
	 * 登录
	 */
	@Override
	public boolean login(Card card) {
		String sql = "select count(*) as num from card where cardnumber='" + card.getCardnumber() + "' and password='"
				+ card.getPassword() + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		int result = 0;
		try {
			if (rs.next()) {
				result = rs.getInt("num");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close();
		}
		if (result == 1)
			return true;
		else
			return false;
	}

	/**
	 * 修改密码
	 */
	@Override
	public boolean updatePsd(String cardnumber, String newPsd) {
		String sql = "update card set password = '" + newPsd + "'where cardnumber='" + cardnumber + "'";
		int result = DbUtil.executeUpdate(sql);
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 存款
	 */
	@Override
	public boolean deposit(String cardnumber, double money) {
		String sql1 = "select balance from card where cardnumber='" + cardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql1);
		double result1 = -1;
		try {
			if (rs.next())
				result1 = rs.getDouble("balance");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (result1 > 0)
			money += result1;
		String sql2 = "update card set balance='" + money + "' where cardnumber='" + cardnumber + "'";
		int result2 = 0;
		result2 = DbUtil.executeUpdate(sql2);
		DbUtil.close();
		if (result2 > 0)
			return true;
		else
			return false;
	}

	/**
	 * 余额
	 */
	@Override
	public boolean withdrawals(String cardnumber, double money) {
		String sql1 = "select balance from card where cardnumber='" + cardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql1);
		double result1 = -1;
		try {
			if (rs.next())
				result1 = rs.getDouble("balance");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (result1 >= money) {
			if (result1 > 0)
				result1 -= money;
			String sql2 = "update card set balance='" + result1 + "' where cardnumber='" + cardnumber + "'";
			int result2 = 0;
			result2 = DbUtil.executeUpdate(sql2);
			DbUtil.close();
			if (result2 > 0)
				return true;
			else
				return false;
		} else
			return false;
	}

	/**
	 * 查询余额
	 */
	@Override
	public double searchBalance(String cardnumber) {
		String sql = "select balance from card where cardnumber='" + cardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		double balance = -1;
		try {
			if (rs.next())
				balance = rs.getDouble("balance");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return balance;
	}

	/**
	 * 查询密码输错次数
	 */
	@Override
	public int getCountByCardnumber(String cardnumber) {
		String sql = "select count from card where cardnumber='" + cardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		int result = 0;
		try {
			if (rs.next())
				result = rs.getInt("count");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return result;
	}

	/**
	 * 修改密码输错次数
	 */
	@Override
	public boolean updateCountByCardnumber(String cardnumber, int count) {
		String sql = "update card set count='" + count + "' where cardnumber='" + cardnumber + "'";
		int result = 0;
		result = DbUtil.executeUpdate(sql);
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 根据交易卡号查询银行卡号
	 */
	public boolean searchCardnumberByTransferCardnumber(String transferCardnumber) {
		String sql = "select cardnumber from card where cardnumber='" + transferCardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		int result = -1;
		try {
			if (rs.next())
				result = rs.findColumn("cardnumber");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 转账
	 */
	public boolean transfer(String transferCardnumber, String cardnumber, double transferBalance) {
		String sql1 = "select balance from card where cardnumber='" + cardnumber + "'";
		String sql2 = "select balance from card where cardnumber='" + transferCardnumber + "'";
		ResultSet rs1 = DbUtil.executeQuery(sql1);
		ResultSet rs2 = DbUtil.executeQuery(sql2);
		double balance1 = -1;
		double balance2 = -1;
		try {
			if (rs1.next())
				balance1 = rs1.getDouble("balance");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (rs2.next())
				balance2 = rs2.getDouble("balance");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (balance1 >= transferBalance) {
			if (balance1 != -1)
				balance1 -= transferBalance;
			if (balance2 != -1)
				balance2 += transferBalance;
			String sql3 = "update card set balance='" + balance1 + "' where cardnumber='" + cardnumber + "'";
			int result1 = DbUtil.executeUpdate(sql3);
			String sql4 = "update card set balance='" + balance2 + "' where cardnumber='" + transferCardnumber + "'";
			int result2 = DbUtil.executeUpdate(sql4);
			if (result1 > 0 && result2 > 0)
				return true;
			else
				return false;
		} else {
			return false;
		}
	}
}
