package dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import bean.User;
import dao.IUserDao;
import util.DbUtil;

/**
 * 用户接口的实现
 * @author ligw
 * @since Sep 9, 2019
 */
public class UserDaoImpl implements IUserDao {
	/**
	 * 根据卡号查询用户个人信息
	 */
	@Override
	public User checkPersonalInfo(String cardnumber) {
		String sql = "select * from user where cardnumber = '" + cardnumber + "'";
		User u = new User();
		ResultSet rs = DbUtil.executeQuery(sql);
		try {
			if (rs.next()) {
				u.setAge(rs.getInt("age"));
				u.setCardnumber(rs.getString("cardnumber"));
				u.setSex(rs.getString("sex"));
				u.setUserid(rs.getString("userid"));
				u.setUsername(rs.getString("username"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return u;
	}

	/**
	 * 根据用户id查询卡号
	 */
	@Override
	public String searchCardnumberByUserid(String userid) {
		String sql = "select cardnumber from user where userid='" + userid + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		String cardnumber = null;
		try {
			if (rs.next())
				cardnumber = rs.getString("cardnumber");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return cardnumber;
	}

}
