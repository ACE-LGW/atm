package dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Card;
import bean.Manager;
import bean.User;
import dao.IManagerDao;
import util.DbUtil;

/**
 * 管理员接口的实现
 * @author ligw
 * @since Sep 9, 2019
 */
public class ManagerDaoImpl implements IManagerDao {
	/**
	 * 管理员登录
	 */
	@Override
	public boolean login(Manager m) {
		String sql = "select count(*) as num from manager where userid='" + m.getUserid() + "' and password='"
				+ m.getPassword() + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		int result = 0;
		try {
			if (rs.next()) {
				result = rs.getInt("num");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		if (result == 1)
			return true;
		else
			return false;
	}

	/**
	 * 添加用户
	 */
	@Override
	public boolean addUser(User u) {
		String sql = "insert into user(userid,username,age,sex,cardnumber) values(?,?,?,?,?)";
		PreparedStatement ps = DbUtil.executePreparedStatement(sql);
		int result = 0;
		try {
			ps.setString(1, u.getUserid());
			ps.setString(2, u.getUsername());
			ps.setInt(3, u.getAge());
			ps.setString(4, u.getSex());
			ps.setString(5, u.getCardnumber());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 添加银行卡
	 */
	@Override
	public boolean addCard(Card c) {
		String sql = "insert into card(cardnumber,password,balance,count) values(?,?,?,?)";
		PreparedStatement ps = DbUtil.executePreparedStatement(sql);
		int result = 0;
		try {
			ps.setString(1, c.getCardnumber());
			ps.setString(2, c.getPassword());
			ps.setDouble(3, c.getBalance());
			ps.setInt(4, c.getCount());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 注销用户和银行卡
	 */
	@Override
	public boolean logout(User u) {
		String sql = "delete from user where userid='" + u.getUserid() + "'";
		String sql1 = "delete from card where cardnumber='" + u.getCardnumber() + "'";
		int result = DbUtil.executeUpdate(sql);
		int result1 = DbUtil.executeUpdate(sql1);
		if (result > 0 && result1 > 0)
			return true;
		else
			return false;
	}

	/**
	 * 查询所有用户
	 */
	@Override
	public List<User> findAll() {
		String sql = "select * from user order by userid";
		List<User> list = new ArrayList<User>();
		ResultSet rs = DbUtil.executeQuery(sql);
		try {
			while (rs.next()) {
				User u = new User();
				u.setUserid(rs.getString("userid"));
				u.setUsername(rs.getString("username"));
				u.setAge(rs.getInt("age"));
				u.setSex(rs.getString("sex"));
				u.setCardnumber(rs.getString("cardnumber"));
				list.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return list;
	}

	/**
	 * 修改密码输错次数，即清零，解冻银行卡
	 */
	@Override
	public boolean updateCountByCardnumber(String cardnumber) {
		String sql = "update card set count='0' where cardnumber='" + cardnumber + "'";
		int result = 0;
		result = DbUtil.executeUpdate(sql);
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 查询所有冻结的银行卡账号
	 */
	@Override
	public List<Card> searchFrozenCard() {
		String sql = "select * from card where count>=3";
		List<Card> list = new ArrayList<Card>();
		ResultSet rs = DbUtil.executeQuery(sql);
		try {
			while (rs.next()) {
				Card c = new Card();
				c.setCardnumber(rs.getString("cardnumber"));
				c.setPassword(rs.getString("password"));
				c.setBalance(rs.getDouble("balance"));
				c.setCount(rs.getInt("count"));
				list.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return list;
	}

	/**
	 * 根据卡号获取银行卡信息
	 */
	@Override
	public Card getCardInfoByCardnumber(String cardnumber) {
		String sql = "select * from card where cardnumber='" + cardnumber + "'";
		ResultSet rs = DbUtil.executeQuery(sql);
		Card c = new Card();
		try {
			while (rs.next()) {
				c.setCardnumber(rs.getString("cardnumber"));
				c.setBalance(rs.getDouble("balance"));
				c.setCount(rs.getInt("count"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return c;
	}
}
