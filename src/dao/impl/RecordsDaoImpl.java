package dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Records;
import dao.IRecordsDao;
import util.DbUtil;

/**
 * 记录接口的实现
 * @author ligw
 * @since Sep 9, 2019
 */
public class RecordsDaoImpl implements IRecordsDao {
	/**
	 * 添加交易记录
	 */
	@Override
	public boolean addRecords(Records r) {
		String sql = "insert into records(exchangetime,exchangetype,exchangemoney,exchangeobject,cardnumber) values(?,?,?,?,?)";
		PreparedStatement ps = DbUtil.executePreparedStatement(sql);
		int result = 0;
		try {
			ps.setString(1, r.getExchangetime());
			ps.setString(2, r.getExchangetype());
			ps.setDouble(3, r.getExchangemoney());
			ps.setString(4, r.getExchangeobject());
			ps.setString(5, r.getCardnumber());
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		if (result > 0)
			return true;
		else
			return false;
	}

	/**
	 * 查询交易记录
	 */
	@Override
	public List<Records> searchRecords(String cardnumber) {
		String sql = "select * from records where cardnumber=" + cardnumber;
		List<Records> list = new ArrayList<Records>();
		ResultSet rs = DbUtil.executeQuery(sql);
		try {
			while (rs.next()) {
				Records r = new Records();
				r.setExchangetime(rs.getString("exchangetime"));
				r.setExchangetype(rs.getString("exchangetype"));
				r.setExchangemoney(rs.getDouble("exchangemoney"));
				r.setExchangeobject(rs.getString("exchangeobject"));
				r.setCardnumber(cardnumber);
				list.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DbUtil.close();
		return list;
	}
}
