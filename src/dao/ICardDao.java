package dao;

import bean.Card;

/**
 * 银行卡的接口
 * @author ligw
 * @since Sep 7, 2019
 */
public interface ICardDao {
	//登录
	public boolean login(Card card);

	//修改密码
	public boolean updatePsd(String cardnumber, String newPsd);

	//存款
	public boolean deposit(String cardnumber, double money);

	//取款
	public boolean withdrawals(String cardnumber, double money);

	//查询余额
	public double searchBalance(String cardnumber);

	//获得该卡号的登录错误次数
	public int getCountByCardnumber(String cardnumber);

	//修改该卡号的登录错误次数
	public boolean updateCountByCardnumber(String cardnumber, int count);

	//通过交易卡号查询银行卡号
	public boolean searchCardnumberByTransferCardnumber(String transferCardnumber);

	//转账
	public boolean transfer(String transferCardnumber, String cardnumber, double transferBalance);
}
