package dao;

import bean.User;

/**
 * 用户的接口
 * @author ligw
 * @since Sep 7, 2019
 */
public interface IUserDao {
	//根据银行卡查询用户信息
	public User checkPersonalInfo(String cardnumber);

	//根据用户id查询用户信息
	public String searchCardnumberByUserid(String userid);
}
