package dao;

import java.util.List;
import bean.Records;

/**
 * 记录的接口
 * @author ligw
 * @since Sep 7, 2019
 */

public interface IRecordsDao {
	//添加银行卡记录
	public boolean addRecords(Records r);

	//查询银行卡记录
	public List<Records> searchRecords(String cardnumber);
}
