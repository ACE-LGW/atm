package factory;

import dao.ICardDao;
import dao.IManagerDao;
import dao.IRecordsDao;
import dao.IUserDao;
import dao.impl.CardDaoImpl;
import dao.impl.ManagerDaoImpl;
import dao.impl.RecordsDaoImpl;
import dao.impl.UserDaoImpl;

/**
 * 工厂类
 * @author ligw
 * @since Sep 9, 2019
 */
public class DaoFactory {
	/**
	 * 用户的接口和接口实现
	 * @return
	 */
	public static IUserDao getUserDaoInstance() {
		return new UserDaoImpl();
	}

	/**
	 * 银行卡的接口和接口实现
	 * @return
	 */
	public static ICardDao getCardDaoInstance() {
		return new CardDaoImpl();
	}

	/**
	 * 管理员的接口和接口实现
	 * @return
	 */
	public static IManagerDao getManagerDaoInstance() {
		return new ManagerDaoImpl();
	}

	/**
	 * 记录的接口和接口实现
	 * @return
	 */
	public static IRecordsDao getRecordsDaoInstance() {
		return new RecordsDaoImpl();
	}
}
