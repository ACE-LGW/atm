package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Records;
import dao.IRecordsDao;
import factory.DaoFactory;

/**
 * RecordsServlet类
 * @author ligw
 * @since Sep 10, 2019
 */
@WebServlet("/card/Records.do")
public class RecordsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * doGet方法，处理get请求
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * doPost方法，处理post请求
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		action(request, response);
	}

	/**
	 * action方法，与前台交互
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设定请求和响应的编码集
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		//获取前台参数method
		String action = request.getParameter("method");
		if (action.equals("addRecords")) {
			//添加记录
			addRecords(request, response);
		}
		if (action.equals("searchRecords")) {
			//查询记录
			searchRecords(request, response);
		}
	}

	/**
	 * 查询记录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void searchRecords(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cardnumber = (String) request.getSession().getAttribute("cardnumber");
		List<Records> list = DaoFactory.getRecordsDaoInstance().searchRecords(cardnumber);
		request.setAttribute("recordslist", list);
		request.getRequestDispatcher("records.jsp").forward(request, response);
	}

	/**
	 * 添加记录
	 * @param request
	 * @param response
	 */
	private void addRecords(HttpServletRequest request, HttpServletResponse response) {
		Records r = (Records) request.getSession().getAttribute("record");
		IRecordsDao recordsDao = DaoFactory.getRecordsDaoInstance();
		try {
			recordsDao.addRecords(r);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
