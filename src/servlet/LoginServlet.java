package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import bean.Card;
import dao.ICardDao;
import factory.DaoFactory;

/**
 * LoginServlet类
 */
@WebServlet("/card/Login.do")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * doGet方法，处理Get请求
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cardnumber = request.getParameter("cardnumber");
		String password = request.getParameter("password");
		Card card = new Card();
		card.setCardnumber(cardnumber);
		card.setPassword(password);
		ICardDao carddao = DaoFactory.getCardDaoInstance();
		int count = carddao.getCountByCardnumber(cardnumber);
		if (count >= 3) {
			JOptionPane.showMessageDialog(null, "您的账号已被冻结！请前往柜台处解冻账号", "Tips", JOptionPane.WARNING_MESSAGE);
			response.sendRedirect("login.jsp");
		} else {
			boolean result = carddao.login(card);
			if (result == false) {
				count++;
				carddao.updateCountByCardnumber(cardnumber, count);
				if ((carddao.getCountByCardnumber(cardnumber)) < 3) {
					JOptionPane.showMessageDialog(null, "输入密码错误！", "Tips", JOptionPane.ERROR_MESSAGE);
					response.sendRedirect("login.jsp");
				} else {
					JOptionPane.showMessageDialog(null, "三次输入密码错误！您的账号已被冻结", "Tips", JOptionPane.WARNING_MESSAGE);
					response.sendRedirect("login.jsp");
				}
			} else {
				request.getSession().setAttribute("cardnumber", cardnumber);
				request.getSession().setAttribute("password", password);
				request.getRequestDispatcher("home.jsp").forward(request, response);
			}
		}

	}

	/**
	 * doPost方法，处理Post请求
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
