package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import bean.Card;
import bean.Manager;
import bean.User;
import dao.IManagerDao;
import dao.IUserDao;
import factory.DaoFactory;

/**
 * ManagerServlet类
 */
@WebServlet("/manager/Manager.do")
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * doGet方法，处理Get请求
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * doPost方法，处理Post请求
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		action(request, response);
	}

	/**
	 * action方法，与前台交互
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设定请求和响应的编码集
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		//获取前台参数method
		String action = request.getParameter("method");
		//根据不同的参数，走不同的方法
		if (action.equals("addUser")) {
			//添加用户
			addUser(request, response);
		} else if (action.equals("login")) {
			//用户登录
			login(request, response);
		} else if (action.equals("addCard")) {
			//添加银行卡
			addCard(request, response);
		} else if (action.equals("logout")) {
			//注销用户和银行卡
			logout(request, response);
		} else if (action.equals("findAll")) {
			//查找所有用户
			findAll(request, response);
		} else if (action.equals("unfreeze")) {
			//解冻用户
			unfreeze(request, response);
		} else if (action.equals("searchFrozenCard")) {
			//查询冻结的银行卡
			searchFrozenCard(request, response);
		} else if (action.equals("getCardInfo")) {
			//查询银行卡信息
			getCardInfo(request, response);
		}
	}

	/**
	 * 获取银行卡信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void getCardInfo(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cardnumber = request.getParameter("cardnumber");
		IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
		Card c = managerdao.getCardInfoByCardnumber(cardnumber);
		if (c == null) {
			JOptionPane.showMessageDialog(null, "获取银行卡信息失败！", "Tips", JOptionPane.ERROR_MESSAGE);
			findAll(request, response);
		} else {
			request.setAttribute("card", c);
			request.getRequestDispatcher("showCardInfo.jsp").forward(request, response);
		}
	}

	/**
	 * 查询冻结的银行卡
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void searchFrozenCard(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Card> list = DaoFactory.getManagerDaoInstance().searchFrozenCard();
		HttpSession session = request.getSession();
		session.setAttribute("cardList", list);
		request.getRequestDispatcher("showFrozenCard.jsp").forward(request, response);
	}

	/**
	 * 解冻银行卡
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	private void unfreeze(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String cardnumber = request.getParameter("cardnumber");
		String userid = request.getParameter("userid");
		IUserDao userdao = DaoFactory.getUserDaoInstance();
		String cardnumber1 = userdao.searchCardnumberByUserid(userid);
		if (cardnumber1 != null) {
			if (!(cardnumber1.equals(cardnumber))) {
				List<Card> list = DaoFactory.getManagerDaoInstance().searchFrozenCard();
				HttpSession session = request.getSession();
				session.setAttribute("cardList", list);
				JOptionPane.showMessageDialog(null, "身份验证出错，无法解冻！", "Tips", JOptionPane.ERROR_MESSAGE);
				request.getRequestDispatcher("showFrozenCard.jsp").forward(request, response);
			} else {
				IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
				if (managerdao.updateCountByCardnumber(cardnumber)) {
					List<Card> list = DaoFactory.getManagerDaoInstance().searchFrozenCard();
					HttpSession session = request.getSession();
					session.setAttribute("cardList", list);
					JOptionPane.showMessageDialog(null, "解冻成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
					request.getRequestDispatcher("showFrozenCard.jsp").forward(request, response);
				} else {
					List<Card> list = DaoFactory.getManagerDaoInstance().searchFrozenCard();
					HttpSession session = request.getSession();
					session.setAttribute("cardList", list);
					JOptionPane.showMessageDialog(null, "解冻失败！", "Tips", JOptionPane.ERROR_MESSAGE);
					request.getRequestDispatcher("showFrozenCard.jsp").forward(request, response);
				}
			}
		} else {
			List<Card> list = DaoFactory.getManagerDaoInstance().searchFrozenCard();
			HttpSession session = request.getSession();
			session.setAttribute("cardList", list);
			JOptionPane.showMessageDialog(null, "不存在此用户！", "Tips", JOptionPane.ERROR_MESSAGE);
			request.getRequestDispatcher("showFrozenCard.jsp").forward(request, response);
		}

	}

	/**
	 * 查找所有用户
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void findAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<User> list = DaoFactory.getManagerDaoInstance().findAll();
		HttpSession session = request.getSession();
		session.setAttribute("userList", list);
		request.getRequestDispatcher("userAdmin.jsp").forward(request, response);
	}

	/**
	 * 注销用户和银行卡
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String userid = request.getParameter("userid");
		String cardnumber = request.getParameter("cardnumber");
		IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
		User u = new User();
		u.setUserid(userid);
		u.setCardnumber(cardnumber);
		if (managerdao.logout(u)) {
			List<User> list = DaoFactory.getManagerDaoInstance().findAll();
			HttpSession session = request.getSession();
			session.setAttribute("userList", list);
			JOptionPane.showMessageDialog(null, "注销成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
			response.sendRedirect("userAdmin.jsp");
		} else {
			List<User> list = DaoFactory.getManagerDaoInstance().findAll();
			HttpSession session = request.getSession();
			session.setAttribute("userList", list);
			JOptionPane.showMessageDialog(null, "注销失败！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("userAdmin.jsp");
		}
	}

	/**
	 * 添加银行卡
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void addCard(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Card c = new Card();
		c.setCardnumber(request.getParameter("cardnumber"));
		c.setPassword(request.getParameter("password"));
		c.setBalance(0.0);
		c.setCount(0);
		IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
		if (managerdao.addCard(c)) {
			JOptionPane.showMessageDialog(null, "添加银行卡成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
			response.sendRedirect("addUser.jsp");
		} else {
			JOptionPane.showMessageDialog(null, "添加银行卡失败！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("addCard.jsp");
		}
	}

	/**
	 * 登录
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Manager m = new Manager();
		String userid = request.getParameter("userid");
		String password = request.getParameter("password");
		m.setUserid(userid);
		m.setPassword(password);
		IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
		if (managerdao.login(m)) {
			response.sendRedirect("manager.jsp");
		} else {
			JOptionPane.showMessageDialog(null, "登录失败！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("manager_login.jsp");
		}
	}

	/**
	 * 添加用户
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void addUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IManagerDao managerdao = DaoFactory.getManagerDaoInstance();
		User u = new User();
		u.setUserid(request.getParameter("userid"));
		u.setAge(Integer.parseInt(request.getParameter("age")));
		u.setSex(request.getParameter("sex"));
		String cardnumber = request.getParameter("cardnumber");
		u.setCardnumber(request.getParameter("cardnumber"));
		u.setUsername(request.getParameter("username"));
		if (managerdao.addUser(u)) {
			request.setAttribute("cardnumber", cardnumber);
			JOptionPane.showMessageDialog(null, "添加用户成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
			request.getRequestDispatcher("addCard.jsp").forward(request, response);
		} else {
			JOptionPane.showMessageDialog(null, "添加用户失败！", "Tips", JOptionPane.ERROR_MESSAGE);
			request.getRequestDispatcher("addUser.jsp").forward(request, response);
		}

	}

}
