package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.User;
import dao.IUserDao;
import factory.DaoFactory;

/**
 * UserServlet类
 * @author ligw
 * @since Sep 10, 2019
 */
@WebServlet("/card/ViewAccount.do")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * doGet方法，处理get请求
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取前台参数method
		String action = request.getParameter("method");
		if ("viewAccount".equals(action)) {
			//查看个人信息
			viewAccount(request, response);
		}
	}

	/**
	 * //查看个人信息
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void viewAccount(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cardnumber = (String) request.getSession().getAttribute("cardnumber");
		IUserDao userDao = DaoFactory.getUserDaoInstance();
		User u = userDao.checkPersonalInfo(cardnumber);
		if (u != null) {
			request.setAttribute("u", u);
			request.getRequestDispatcher("showAccount.jsp").forward(request, response);
		}
	}

	/**
	 * doPost方法，处理post请求
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
