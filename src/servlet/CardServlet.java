package servlet;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import bean.Records;
import bean.User;
import dao.ICardDao;
import dao.IRecordsDao;
import dao.IUserDao;
import factory.DaoFactory;

/**
 * CardServlet类
 */
@WebServlet("/card/getMessage.do")
public class CardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * doGet方法，处理Get请求
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * action方法，与前台交互
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void action(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取前台参数method
		String action = request.getParameter("method");
		//根据不同的参数，走不同的方法
		if ("searchBalance".equals(action)) {
			//查询余额
			search(request, response);
		} else if ("withdrawals".equals(action)) {
			//取款
			withdrawals(request, response);
		} else if ("deposit".equals(action)) {
			//存款
			deposit(request, response);
		} else if ("transfer".equals(action)) {
			//转账
			transfer(request, response);
		} else if ("updatepwd".equals(action)) {
			//修改密码
			updatepwd(request, response);
		}
	}

	/**
	 * 转账
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void transfer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//从前台获取转账卡号和转账金额
		String transferCardnumber = (String) request.getParameter("transferCardnumber");
		String transferBalance = (String) request.getParameter("transferBalance");

		double m = Double.parseDouble(transferBalance);
		DecimalFormat d = new DecimalFormat("#.00");
		transferBalance = d.format(m);
		m = Double.parseDouble(transferBalance);
		String cardnumber = (String) request.getSession().getAttribute("cardnumber");
		ICardDao carddao = DaoFactory.getCardDaoInstance();
		if (carddao.searchCardnumberByTransferCardnumber(transferCardnumber)) {
			if (carddao.transfer(transferCardnumber, cardnumber, m)) {
				//	获取系统时间
				Date now = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");// 可以方便地修改日期格式
				String time = dateFormat.format(now);
				//保存交易记录
				Records r = new Records();
				r.setExchangetime(time);
				r.setExchangetype("转账");
				r.setExchangemoney(m);
				r.setExchangeobject(transferCardnumber);
				r.setCardnumber(cardnumber);
				IRecordsDao rdao = DaoFactory.getRecordsDaoInstance();
				rdao.addRecords(r);

				JOptionPane.showMessageDialog(null, "转账成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
				request.setAttribute("transferCardnumber", transferCardnumber);
				request.setAttribute("transferBalance", transferBalance);
				request.setAttribute("time", time);
				request.getRequestDispatcher("successfulTransfer.jsp").forward(request, response);
			} else {
				JOptionPane.showMessageDialog(null, "转账金额超过余额！", "Tips", JOptionPane.ERROR_MESSAGE);
				response.sendRedirect("transfer.jsp");
			}
		} else {
			JOptionPane.showMessageDialog(null, "转账银行卡号不存在！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("transfer.jsp");
		}
	}

	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void updatepwd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cardnumber = (String) request.getSession().getAttribute("cardnumber");
		String originalPsd = request.getParameter("Confirm_Password2");

		ICardDao cardDao = DaoFactory.getCardDaoInstance();
		boolean result = cardDao.updatePsd(cardnumber, originalPsd);
		if (result == false) {
			JOptionPane.showMessageDialog(null, "修改密码失败，详情请咨询前台工作人员！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("updatepwd.jsp");
		} else {
			JOptionPane.showMessageDialog(null, "修改密码成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
			request.getRequestDispatcher("home.jsp").forward(request, response);
		}
	}

	/**
	 * 存款
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void deposit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String money = (String) request.getParameter("money");
		double balance = -1;
		double m = Double.parseDouble(money);
		DecimalFormat d = new DecimalFormat("#.00");// 输入的存款金额保留两位小数
		money = d.format(m);
		m = Double.parseDouble(money);
		if ((m % 100) == 0) {
			if (m <= 10000) {
				String cardnumber = (String) request.getSession().getAttribute("cardnumber");
				ICardDao carddao = DaoFactory.getCardDaoInstance();
				if (carddao.deposit(cardnumber, m)) {
					//获取系统时间
					Date date = new Date();
					SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String times = from.format(date);
					//保存存款记录
					Records r = new Records();
					r.setExchangetime(times);
					r.setExchangetype("存款");
					r.setExchangemoney(m);
					r.setExchangeobject(cardnumber);
					r.setCardnumber(cardnumber);
					IRecordsDao rdao = DaoFactory.getRecordsDaoInstance();
					rdao.addRecords(r);

					balance = carddao.searchBalance(cardnumber);
					request.setAttribute("balance", String.valueOf(balance));
					request.setAttribute("money", String.valueOf(m));
					JOptionPane.showMessageDialog(null, "存款成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
					request.getRequestDispatcher("deposit_success.jsp").forward(request, response);
				} else {
					JOptionPane.showMessageDialog(null, "存款失败！", "Tips", JOptionPane.ERROR_MESSAGE);
					response.sendRedirect("deposit.jsp");
				}
			} else {
				JOptionPane.showMessageDialog(null, "每次存款金额不能超过10,000元！", "Tips", JOptionPane.ERROR_MESSAGE);
				response.sendRedirect("deposit.jsp");
			}
		} else {
			JOptionPane.showMessageDialog(null, "存款金额必须为整百！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("deposit.jsp");
		}

	}

	/**
	 * 取款
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	private void withdrawals(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String m = request.getParameter("money");
		double money = Double.parseDouble(m);
		DecimalFormat d = new DecimalFormat("#.00");// 输入的取款金额保留两位小数
		m = d.format(money);
		money = Double.parseDouble(m);
		if (money % 100 == 0) {
			if (money <= 10000) {
				String cardnumber = (String) request.getSession().getAttribute("cardnumber");
				ICardDao carddao = DaoFactory.getCardDaoInstance();
				if (carddao.withdrawals(cardnumber, money)) {
					//					获取系统时间
					Date date = new Date();
					SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String times = from.format(date);
					//保存交易记录
					Records r = new Records();
					r.setExchangetime(times);
					r.setExchangetype("取款");
					r.setExchangemoney(money);
					r.setExchangeobject(cardnumber);
					r.setCardnumber(cardnumber);
					IRecordsDao rdao = DaoFactory.getRecordsDaoInstance();
					rdao.addRecords(r);

					double balance = carddao.searchBalance(cardnumber);
					request.setAttribute("balance", String.valueOf(balance));
					JOptionPane.showMessageDialog(null, "取款成功！", "Tips", JOptionPane.INFORMATION_MESSAGE);
					request.getRequestDispatcher("withdrawals_success.jsp").forward(request, response);
				} else {
					JOptionPane.showMessageDialog(null, "余额不足！", "Tips", JOptionPane.ERROR_MESSAGE);
					response.sendRedirect("withdrawals.jsp");
				}
			} else {
				JOptionPane.showMessageDialog(null, "每次取款金额不能超过10,000元！", "Tips", JOptionPane.ERROR_MESSAGE);
				response.sendRedirect("withdrawals.jsp");
			}

		} else {
			JOptionPane.showMessageDialog(null, "取款金额必须为整百！", "Tips", JOptionPane.ERROR_MESSAGE);
			response.sendRedirect("withdrawals.jsp");
		}

	}

	/**
	 * 查询余额
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cardnumber = (String) request.getSession().getAttribute("cardnumber");
		IUserDao userDao = DaoFactory.getUserDaoInstance();
		User u = userDao.checkPersonalInfo(cardnumber);

		request.setAttribute("u", u);
		ICardDao carddao = DaoFactory.getCardDaoInstance();
		double balance = carddao.searchBalance(cardnumber);
		request.setAttribute("balance", String.valueOf(balance));
		request.getRequestDispatcher("showBalance.jsp").forward(request, response);

	}

	/**
	 * doGet方法，处理Post请求
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		action(request, response);
	}

}
