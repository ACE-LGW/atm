package util;

import java.sql.*;

/**
 * JDBC工具类
 * @author ligw
 * @since Sep 10, 2019
 */
public class DbUtil {
	/**
	 * jdbc连接的url、user、password
	 */
	private static final String URL = "jdbc:mysql://localhost/bank(1)";

	private static final String USER = "root";

	private static final String PASSWORD = "ligw";

	/**
	 * 连接jdbc的参数
	 */
	protected static Statement s = null;

	protected static ResultSet rs = null;

	protected static Connection conn = null;

	/**
	 * jdbc注册驱动、建立连接
	 * @return
	 */
	public static synchronized Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * executeUpdate执行修改、插入、删除sql语句
	 * @param sql
	 * @return
	 */
	public static int executeUpdate(String sql) {
		int result = 0;
		try {
			s = getConnection().createStatement();
			result = s.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * executeQuery执行查询sql语句
	 * @param sql
	 * @return
	 */
	public static ResultSet executeQuery(String sql) {

		try {
			s = getConnection().createStatement();
			rs = s.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	/**
	 * PreparedStatement预处理，安全执行sql语句
	 * @param sql
	 * @return
	 */
	public static PreparedStatement executePreparedStatement(String sql) {
		PreparedStatement ps = null;
		try {
			ps = getConnection().prepareStatement(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ps;
	}

	/**
	 * 回滚操作
	 */
	public static void rollback() {
		try {
			getConnection().rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 关闭资源
	 */
	public static void close() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (s != null) {
				s.close();
			}
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
