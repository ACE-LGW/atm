package bean;

/**
 * 用户实体类
 * @author ligw
 * @since Sep 7, 2019
 */
public class User {
	private String userid;//用户id

	private String username;//用户名

	private int age;//用户年龄

	private String sex;//用户性别

	private String cardnumber;//银行卡号

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
}
