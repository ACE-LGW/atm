package bean;

/**
 * 管理员实体类
 * @author ligw
 * @since Sep 7, 2019
 */
public class Manager {
	private String userid;//管理员id

	private String password;//管理员密码

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
