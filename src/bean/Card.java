package bean;

/**
 * 银行卡的实体类
 * @author ligw
 * @since Sep 7, 2019
 */
public class Card {
	private String cardnumber;//银行卡号

	private Double balance;//余额

	private String password;//密码

	private int count;//输错次数

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
