package bean;

/**
 * 记录的实体类
 * @author 10234
 * @since Sep 7, 2019
 */
public class Records {
	private String exchangetime;//交易时间

	private String exchangetype;//交易类型

	private double exchangemoney;//交易金额

	private String exchangeobject;//交易对象

	private String cardnumber;//银行卡号

	public String getExchangetime() {
		return exchangetime;
	}

	public void setExchangetime(String exchangetime) {
		this.exchangetime = exchangetime;
	}

	public String getExchangetype() {
		return exchangetype;
	}

	public void setExchangetype(String exchangetype) {
		this.exchangetype = exchangetype;
	}

	public double getExchangemoney() {
		return exchangemoney;
	}

	public void setExchangemoney(double exchangemoney) {
		this.exchangemoney = exchangemoney;
	}

	public String getExchangeobject() {
		return exchangeobject;
	}

	public void setExchangeobject(String exchangeobject) {
		this.exchangeobject = exchangeobject;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
}
