<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>transfer</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/time.js"></script>
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<form action="getMessage.do?method=transfer" method="post">
				<table align="center" frame="box" width="500" height="300"
					id="table">
					<tr>
						<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;转账卡号：
						</td>
						<td align="left"><input type="text" name="transferCardnumber"></td>
					</tr>
					<tr>
						<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;转账金额：</td>
						<td align="left"><input type="text" name="transferBalance"></td>
					</tr>
					<tr>
						<td align="left"><input type="submit" name="submit"
							value="确认" class="button" /></td>
						<td></td>
						<td align="right"><input type="button" name="cancle"
							value="取消" onclick="window.location.href='home.jsp'"
							class="button" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>