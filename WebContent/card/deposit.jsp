<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"
	import="bean.User,dao.IUserDao,dao.impl.UserDaoImpl,factory.DaoFactory"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>deposit</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/time.js"></script>
</head>
<%
	String cardnumber = (String) request.getSession().getAttribute("cardnumber");
	IUserDao userDao = DaoFactory.getUserDaoInstance();
	User u = userDao.checkPersonalInfo(cardnumber);
%>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<form action="getMessage.do?method=deposit" method="post">
				<table frame="box" align="center" width="500" height="300"
					id="table">
					<tr>
						<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;姓名：</td>
						<td align="left"><%=u.getUsername()%></td>
					</tr>
					<tr>

						<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卡号：</td>
						<td align="left"><%=cardnumber%></td>
					</tr>
					<tr>
						<td align="center" colspan="2">存款金额： <input type="text"
							name="money" /></td>

					</tr>
					<tr>
						<td align="left"><input type="submit" name="submit"
							value="确定" class="button" /></td>
						<td align="right"><input type="button" name="enter"
							value="返回主菜单" onclick="location.href='home.jsp'" class="button" /></td>
					</tr>
				</table>
		</div>
	</div>
	</form>
</body>
</html>