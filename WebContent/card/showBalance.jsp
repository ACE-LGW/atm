<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>showBalance</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/time.js"></script>
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<%
				User u = (User) request.getAttribute("u");
				String balance = (String) request.getAttribute("balance");
			%>
			<table align="center" frame="box" width="500" height="300" id="table">
				<tr>
					<td></td>
					<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;姓名：
						<%=u.getUsername()%></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;卡号：
						<%=u.getCardnumber()%></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;余额：
						<%=balance%></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="exit" value="退卡"
						onclick="window.location.href='login.jsp'" class="button" /></td>
					<td></td>
					<td align="right"><input type="button" name="continue"
						value="继续交易" onclick="window.location.href='home.jsp'"
						class="button" /></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>