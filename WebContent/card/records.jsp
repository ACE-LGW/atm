<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.Records"
	import="bean.Card,java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>showRecords</title>
<script type="text/javascript" src="../js/time.js"></script>
<link rel="stylesheet" type="text/css" href="../css/records.css" />
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />
		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<table align="center" frame="box" width="800" height="300" id="table"
				border="1">
				<thead>
					<tr>
						<th>银行卡号</th>
						<th>交易时间</th>
						<th>交易类型</th>
						<th>交易金额</th>
						<th>交易对象</th>
					</tr>
				</thead>
				<tbody>
					<%
						request.setCharacterEncoding("UTF-8");
						List<Records> list;
						list = (List<Records>) request.getAttribute("recordslist");
						for (Records r : list) {
					%>
					<tr>
						<td><%=r.getCardnumber()%>
						<td><%=r.getExchangetime()%></td>
						<td><%=r.getExchangetype()%></td>
						<td><%=r.getExchangemoney()%></td>
						<td><%=r.getExchangeobject()%></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
			<table align="center" width="800" height="100">
				<tr>
					<td align="center"><input type="button" name="exit" value="退卡"
						onclick="window.location.href='login.jsp'" class="button" /></td>
					<td align="center"><input type="button" name="continue"
						value="继续交易" onclick="window.location.href='home.jsp'"
						class="button" /></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>