<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/updatepwd.js"></script>
<script type="text/javascript" src="../js/time.js"></script>
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<form method="post" action="getMessage.do?method=updatepwd">
				<table align="center" frame="box" width="500" height="300"
					id="table">
					<tr>
						<td align="center" colspan="2">新密码：&nbsp;&nbsp;&nbsp; <input
							type="password" name="Confirm_Password" id="pwd1"
							onBlur="return checkForm()"></td>
						<td><span id="tips_password">* 密码由6位数字组成</span></td>
					</tr>
					<tr>
						<td align="center" colspan="2">确认密码： <input type="password"
							name="Confirm_Password2" id="pwd2" onBlur="return checkForm()"></td>
						<td><span id="tips_repeat">* 请再次输入你的密码</span></td>
					</tr>
					<tr>
						<td align="center"><input type="submit" value="修改"
							onclick="return checkForm()" class="button" /></td>
						<td align="center" colspan="2"><input type="reset" value="重置"
							class="button" /></td>
					</tr>
					<tr>
						<td align="right" colspan="2"><input type="button" value="取消"
							onclick="location.href='home.jsp'" class="button" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>