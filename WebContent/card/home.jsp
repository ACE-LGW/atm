<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>home</title>
<script type="text/javascript" src="../js/time.js"></script>
<link rel="stylesheet" type="text/css" href="../css/home.css" />
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #00ffe7">60秒</font><br />
			</div>
			<table align="center" width="500" height="300" frame="box">
				<tr>
					<td align="left"><input type="button" name="balance"
						class="button" value="余额"
						onClick="location.href='getMessage.do?method=searchBalance'">
					</td>
					<td align="right"><input type="button" name="deposit"
						class="button" value="存款" onclick="location.href='deposit.jsp'"></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="account"
						class="button" value="账户"
						onClick="location.href='ViewAccount.do?method=viewAccount'">
					</td>
					<td align="right"><input type="button" name="withdrawals"
						class="button" value="取款"
						onclick="location.href='withdrawals.jsp'"></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="updatePsd"
						class="button" value="修改密码"
						onclick="location.href='updatepwd.jsp'"></td>
					<td align="right"><input type="button" name="transfer"
						class="button" value="转账" onclick="location.href='transfer.jsp'"></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="searchRecords"
						class="button" value="交易记录"
						onclick="location.href='Records.do?method=searchRecords'"></td>
					<td align="right" colspan="2"><input type="button"
						name="exits" value="退出" class="button"
						onclick="window.location.href='login.jsp'"></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>