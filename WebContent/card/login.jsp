<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>login</title>
<link rel="stylesheet" type="text/css" href="../css/login.css" />
<script type="text/javascript" src="../js/login.js"></script>
</head>
<body>
	<div class="img">
		<img src="../images/10.png" />
	</div>

	<div class="center">
		<form action="Login.do" method="post">
			<table id="table" width="500" height="300">
				<tr>
					<td align="right">卡号：</td>
					<td align="left"><input type="text" name="cardnumber"
						id="txtcardnumber" onBlur="checkForm()"></td>
					<td align="right"><span id="tips_cardnumber">*
							卡号由19位数字组成 *</span></td>
				</tr>
				<tr>
					<td align="right">密码：</td>
					<td align="left"><input type="password" name="password"
						id="txtpwd" onBlur="checkForm()"></td>
					<td align="right"><span id="tips_pwd">* 密码由6位数字组成 *</span></td>
				</tr>
				<tr>
					<td align="center" colspan="3"><input type="submit"
						name="submit" value="登录" class="button" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
						type="reset" name="reset" value="重置" class="button" /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>