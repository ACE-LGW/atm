<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>successfulTransfer</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/time.js"></script>
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<%
				String transferCardnumber = (String) request.getAttribute("transferCardnumber");
				String transferBalance = (String) request.getAttribute("transferBalance");
				String time = (String) request.getAttribute("time");
			%>
			<table align="center" frame="box" width="500" height="300" id="table">
				<td align="center" colspan="2">转账成功！</td>
				<tr>
					<td></td>
					<td align="left">转账卡号： <%=transferCardnumber%>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="left">转账金额： <%=transferBalance%></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="left">转账时间：<%=time%>
					</td>
					<td></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="exit" value="退卡"
						onclick="window.location.href='login.jsp'" class="button" /></td>
					<td align="right"><input type="button" name="continue"
						value="继续交易" onclick="window.location.href='home.jsp'"
						class="button" /></td>

				</tr>
			</table>
		</div>
	</div>
</body>
</html>