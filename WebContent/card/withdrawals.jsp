<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>withdrawals</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript">
	function getMoney(name) {
		document.getElementsByName("money")[0].value = "";
		var v = document.getElementsByName(name)[0].value;
		document.getElementsByName("money")[0].value += v;
		var passwordbank="<%=request.getSession().getAttribute("password")%>";
	}
</script>
<script type="text/javascript" src="../js/time.js"></script>
</head>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<form action="getMessage.do?method=withdrawals" method="post">
				<table align="center" frame="box" width="500" height="300"
					id="table">
					<tr>
						<td align="left"><input type="button" name="100" value="100"
							onClick="getMoney(this.name)" class="btn" /></td>
						<td></td>
						<td></td>
						<td align="right"><input type="button" name="1000"
							value="1000" onClick="getMoney(this.name)" class="btn" /></td>
					</tr>
					<tr>
						<td align="left"><input type="button" name="200" value="200"
							onClick="getMoney(this.name)" class="btn" /></td>
						<td align="center" colspan="2">请输入您的取款金额：<input type="text"
							name="money" id="money" /></td>
						<td align="right"><input type="button" name="1500"
							value="1500" onClick="getMoney(this.name)" class="btn" /></td>
					</tr>
					<tr>
						<td align="left" colspan="2"><input type="button" name="500"
							value="500" onClick="getMoney(this.name)" class="btn" /></td>
						<td align="right" colspan="2"><input type="button"
							name="2000" value="2000" onClick="getMoney(this.name)"
							class="btn" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2"><input type="submit"
							name="enter" value="确定" class="button" /></td>
						<td align="center" colspan="2"><input type="button"
							name="cancel" value="取消" onclick="location.href='home.jsp'"
							class="button" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>