<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>success</title>
<link rel="stylesheet" type="text/css" href="../css/show.css" />
<script type="text/javascript" src="../js/time.js"></script>
</head>
<%
	String balance = (String) request.getAttribute("balance");
	String money = (String) request.getAttribute("money");
	String cardnumber = (String) request.getSession().getAttribute("cardnumber");
%>
<body onload="window.setInterval(shua,1000);">
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<div class="time">
				<font id="hints" style="color: #FFEB3B">60秒</font><br />
			</div>
			<table frame="box" width="500" height="300" align="center" id="table">
				<tr>
					<td align="right">当前账号：</td>
					<td align="left"><%=cardnumber%></td>
				</tr>
				<tr>
					<td align="right">存款金额：</td>
					<td align="left"><%=money%></td>
				</tr>
				<tr>
					<td align="right">当前余额：</td>
					<td align="left"><%=balance%></td>
				</tr>
				<tr>
					<td align="left"><input type="button" name="exit" value="退卡"
						onclick="location.href='login.jsp'" class="button" /></td>
					<td align="right"><input type="button" name="return"
						value="继续交易" onclick="location.href='home.jsp'" class="button" /></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>