<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>manager_login</title>
<link rel="stylesheet" type="text/css" href="../css/login.css" />
<script type="text/javascript" src="../js/manager_login.js"></script>
</head>
<body>
	<div class="img">
		<img src="../images/10.png" />
	</div>
	<div class="center">
		<form action="Manager.do?method=login" method="post">
			<table id="table" width="500" height="300">
				<tr>
					<td align="right">用户：</td>
					<td align="left"><input type="text" name="userid"
						id="txtuserid" onBlur="return checkForm()" /></td>
					<td align="left"><span id="tips_userid">* 用户名由2位数字组成 *</span></td>
				</tr>
				<tr>
					<td align="right">密码：</td>
					<td align="left"><input type="password" name="password"
						id="txtPwd" onBlur="return checkForm()" /></td>
					<td align="left"><span id="tips_Pwd">* 密码由8位数字组成 *</span></td>
				</tr>
				<tr>
					<td align="center" colspan="3"><input type="submit"
						name="submit" value="登录" class="button" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
						type="reset" name="reset" value="重置" class="button" /></td>
				</tr>

			</table>

		</form>
	</div>
</body>
</html>