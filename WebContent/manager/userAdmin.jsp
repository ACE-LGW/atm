<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.User,java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>userAdmin</title>
<link href="../css/table.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="center">
		<form>
			<table border="1" id="table">
				<thead>
					<tr>
						<th>身份证号</th>
						<th>姓名</th>
						<th>年龄</th>
						<th>性别</th>
						<th>银行卡号</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<%
						request.setCharacterEncoding("UTF-8");
					%>
					<%
						List<User> list;
						list = (List) session.getAttribute("userList");
						for (User u : list) {
					%>

					<tr>
						<td><%=u.getUserid()%></td>
						<td><%=u.getUsername()%></td>
						<td><%=u.getAge()%></td>
						<td><%=u.getSex()%></td>
						<td><a
							href='Manager.do?cardnumber=<%=u.getCardnumber()%>&method=getCardInfo'><%=u.getCardnumber()%></a></td>
						<td><a
							href="Manager.do?userid=<%=u.getUserid()%>&cardnumber=<%=u.getCardnumber()%>&method=logout">注销</a>
						</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>