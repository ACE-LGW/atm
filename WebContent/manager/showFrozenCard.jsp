<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.Card,java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>showFrozenCard</title>
<link href="../css/table.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="center">
		<form>
			<table border="1" id="table">
				<thead>
					<tr>
						<th>卡号</th>
						<th>密码</th>
						<th>余额</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<%
						request.setCharacterEncoding("UTF-8");
					%>
					<%
						List<Card> list;
						list = (List) session.getAttribute("cardList");
						for (Card c : list) {
					%>
					<tr>
						<td><%=c.getCardnumber()%></td>
						<td><%=c.getPassword()%></td>
						<td><%=c.getBalance()%></td>
						<td><a href="verify.jsp?cardnumber=<%=c.getCardnumber()%>">解冻</a>
						</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>