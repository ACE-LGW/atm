<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" import="bean.Card"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>showCardInfo</title>
<link href="../css/table.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="center">
		<form>
			<table align="center" border="1" id="table">
				<thead>

					<tr>
						<th>银行卡号</th>
						<th>余额</th>
						<th>状态</th>
					</tr>

				</thead>
				<tbody>
					<%
						request.setCharacterEncoding("utf-8");
						Card c = (Card) request.getAttribute("card");
					%>
					<tr>
						<td><%=c.getCardnumber()%></td>
						<td><%=c.getBalance()%></td>
						<td><%=c.getCount()%></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>