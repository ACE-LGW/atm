<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>addUser</title>
<link rel="stylesheet" href="../css/manager.css" type="text/css" />
<script type="text/javascript" src="../js/addUser.js"></script>
</head>
<body>
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<form action="Manager.do?method=addUser" method="post">
				<table frame="box" align="center" width="500" height="300"
					id="table">
					<tr>
						<td align="right">身份证号：</td>
						<td align="left"><input type="text" name="userid"
							id="txtuserid" onBlur="return checkForm()" /></td>
						<td align="left"><span id="tips_userid">* 身份证由18位数字组成
								*</span></td>
					</tr>
					<tr>
						<td align="right">用户姓名：</td>
						<td align="left"><input type="text" name="username"
							id="txtusername" onBlur="return checkForm()" /></td>
						<td align="left"><span id="tips_username">* 必须填写真实姓名 *</span></td>
					</tr>
					<tr>
						<td align="right">银行卡号：</td>
						<td align="left"><textarea name="cardnumber" cols="20"
								rows="1" id="txtcardnumber" onBlur="return checkForm()"></textarea></td>
						<td align="left"><span id="tips_cardnumber">*
								银行卡号由19位数字组成 *</span></td>
					</tr>
					<tr>
						<td align="right">用户年龄：</td>
						<td align="left"><input type="text" name="age" /></td>
					</tr>
					<tr>
						<td align="right">用户性别：</td>
						<td align="left"><input name="sex" type="radio" value="男" />男&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="sex" type="radio" value="女" />女</td>
					</tr>

					<tr>
						<td align="center" colspan="2"><input type="submit"
							name="submit" value="确定" class="button" />&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							type="reset" name="reset" value="重置" class="button" /></td>
					</tr>

				</table>
			</form>
		</div>
	</div>
</body>
</html>