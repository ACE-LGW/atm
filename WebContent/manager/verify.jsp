<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>verify</title>
<link rel="stylesheet" href="../css/manager.css" type="text/css" />
<script type="text/javascript" src="../js/verify.js"></script>
</head>
<%
	String cardnumber = request.getParameter("cardnumber");
%>
<body>
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<form action="Manager.do?method=unfreeze" method="post">
				<table frame="box" width="500" height="300" id="table">
					<tr>
						<td align="right">银行卡号：</td>
						<td align="left"><input type="text" name="cardnumber"
							value="<%=cardnumber%>" readonly="readonly" /></td>

					</tr>
					<tr>
						<td align="right">身份证号：</td>
						<td align="left"><input type="text" name="userid"
							id="txtuserid" onBlur="return checkForm()" /></td>
						<td align="left"><span id="tips_userid">* 身份证由18位数字组成
								*</span></td>
					</tr>
					<tr>
						<td align="left"><input type="submit" name="submit"
							value="确定" class="button" /></td>
						<td align="right" colspan="2"><input type="reset"
							name="reset" value="重置" class="button" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>