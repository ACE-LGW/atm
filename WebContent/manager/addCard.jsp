<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>addCard</title>
<link rel="stylesheet" href="../css/manager.css" type="text/css" />
<script type="text/javascript" src="../js/addCard.js"></script>
</head>
<%
	String cardnumber = (String) request.getAttribute("cardnumber");
%>
<body>
	<div class="img">
		<img src="../images/10.png" />

		<div class="center">
			<form action="Manager.do?method=addCard" method="post">
				<table frame="box" width="500" height="300" id="table"
					align="center">
					<tr>
						<td>卡号：</td>
						<td align="left"><input type="text" name="cardnumber"
							value="<%=cardnumber%>" readonly="readonly" /></td>
					</tr>
					<tr>
						<td>设置密码：</td>
						<td align="left"><input type="password" name="password"
							id="pwd1" onBlur="return checkForm()" /></td>
						<td><span id="tips_password">* 密码由6位数字组成</span></td>
					</tr>
					<tr>
						<td align="center" colspan="3"><input type="submit"
							name="submit" value="确定" class="button" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							type="reset" name="reset" value="重置" class="button" /></td>
					</tr>

				</table>
			</form>
		</div>
	</div>
</body>
</html>