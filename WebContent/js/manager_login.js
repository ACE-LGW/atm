function checkForm() {
	if (document.getElementById("txtuserid").value.length != 2) {
		document.getElementById("tips_userid").innerHTML = "<em style='color:#FF0000'>用户名由2位数字组成</em>";
		document.getElementById("txtuserid").focus();
		return false;
	} else {
		document.getElementById("tips_userid").innerHTML = "OK!</em>";
	}
	if (document.getElementById("txtPwd").value.length != 8) {
		document.getElementById("tips_Pwd").innerHTML = "<em style='color:#FF0000'>密码由8位数字组成</em>";
		document.getElementById("txtPwd").focus();
		return false;
	} else {
		document.getElementById("tips_Pwd").innerHTML = "OK!</em>";
	}
}
