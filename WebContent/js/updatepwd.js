/*校验密码*/
function checkForm() {
	// 获取表单信息
	var confirm_password = document.getElementById("pwd1").value;
	var confirm_password2 = document.getElementById("pwd2").value;
	// 判断用户是否输入的完整的信息
	if (confirm_password2 == "" || confirm_password == "") {
		alert("所有信息必须填写完整");
		return false;
	}
	if (document.getElementById("pwd1").value.length != 6) {
		document.getElementById("tips_password").innerHTML = "<em style='color:#FF0000'>密码由6位数字组成</em>";
		document.getElementById("pwd1").focus();
		return false;
	} else {
		document.getElementById("tips_password").innerHTML = "OK！";
	}
	if (document.getElementById("pwd2").value != document.getElementById("pwd1").value) {
		document.getElementById("tips_repeat").innerHTML = "<em style='color:#FF0000'>两次输入的密码不一致</em>";
		document.getElementById("pwd2").focus();
		return false;
	} else {
		document.getElementById("tips_repeat").innerHTML = "OK！";

	}
}