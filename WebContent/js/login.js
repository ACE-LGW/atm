/*
检查卡号是否正确
*/
function checkForm() {
	if (document.getElementById("txtcardnumber").value.length != 19) {
		document.getElementById("tips_cardnumber").innerHTML = "<em style='color:#FF0000'>卡号由19位数字组成</em>";
		document.getElementById("txtcardnumber").focus();
		//return false;
	} else {
		document.getElementById("tips_cardnumber").innerHTML = "OK!</em>";
	}
	if (document.getElementById("txtpwd").value.length != 6) {
		document.getElementById("tips_pwd").innerHTML = "<em style='color:#FF0000'>密码由6位数字组成</em>";
		document.getElementById("txtpwd").focus();
		//return false;
	} else {
		document.getElementById("tips_pwd").innerHTML = "OK!</em>";
	}
}
