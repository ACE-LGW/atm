/*检验身份证、姓名、银行卡号*/
function checkForm() {
	if (document.getElementById("txtuserid").value.length != 18) {
		document.getElementById("tips_userid").innerHTML = "<em style='color:#FF0000'>身份证号由18位数字组成</em>";
		document.getElementById("txtuserid").focus();
		return false;
	} else {
		document.getElementById("tips_userid").innerHTML = "OK!</em>";
	}
	if (document.getElementById("txtusername").value.length == "") {
		document.getElementById("tips_username").innerHTML = "<em style='color:#FF0000'>必须填写真实姓名</em>";
		document.getElementById("txtusername").focus();
		return false;
	} else {
		document.getElementById("tips_username").innerHTML = "OK!</em>";
	}
	if (document.getElementById("txtcardnumber").value.length != 19) {
		document.getElementById("tips_cardnumber").innerHTML = "<em style='color:#FF0000'>卡号由19位数字组成</em>";
		document.getElementById("txtcardnumber").focus();
		return false;
	} else {
		document.getElementById("tips_cardnumber").innerHTML = "OK!</em>";
	}
}